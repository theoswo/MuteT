#pragma semicolon 1

#include <sourcemod>
#include <basecomm>

#define PLUGIN_VERSION "1.2"

public Plugin myinfo = 
{
	name = "Hosties - Mute T",
	author = "Oscar Wos (OSWO)",
	description = "Hosties Extended Plugin",
	version = PLUGIN_VERSION,
	url = "www.tangoworldwide.net",
};

ConVar g_Enabled;
int TeamSwitches[MAXPLAYERS + 1];
int g_ConvarInt;

public void OnPluginStart()
{
	RegConsoleCmd("jointeam", event_Join);
	HookEvent("player_team", event_PTeam);
	
	g_Enabled = CreateConVar("sm_mutet", "0", "Sets whether to mute T's or not", _, true, 0.0, true, 1.0);
	g_ConvarInt = GetConVarInt(g_Enabled);
	HookConVarChange(g_Enabled, ConvarChanged);
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
			MutePlayer(i);
	}
}

public Action event_PTeam(Event event, char[] name, bool dontBroadcast)
{
	int client = event.GetInt("userid");
	MutePlayer(client);
}

public void ConvarChanged(ConVar convar, char[] oldValue, char[] newValue)
{
	g_ConvarInt = GetConVarInt(g_Enabled);
	
	if (StringToInt(newValue) == 1)
	{
		PrintToChatAll("[SM] T's are now not able to talk... Forever!");
		
		for (int i = 1; i < MaxClients; i++)
		{
			if (IsValidClient(i))
				MutePlayer(i);
		}
	}
	else
	{
		PrintToChatAll("[SM] T's are now able to talk!");
		
		for (int i = 1; i < MaxClients; i++)
		{
			if (IsValidClient(i))
				MutePlayer(i);
		}
	}
}

public Action event_Join(int client, int args)
{
	if (IsValidClient(client))
	{
		TeamSwitches[client]++;
		
		if (TeamSwitches[client] > 1)
		{
			PrintToChat(client, "[SM] You can only switch team once! Try reconnecting!");
			return Plugin_Handled;
		} else {
			char c_TeamArg[4];
			GetCmdArg(1, c_TeamArg, sizeof(c_TeamArg));
			int c_TeamNum = StringToInt(c_TeamArg);
			
			ChangeClientTeam(client, c_TeamNum);
			MutePlayer(client);
		}
	}
	
	return Plugin_Handled;
}

public void OnMapEnd()
{
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
			TeamSwitches[i] = 0;
	}
}

public void OnClientDisconnect(client)
{
	TeamSwitches[client] = 0;
}

public void MutePlayer(client)
{	
	if (g_ConvarInt == 1)
	{
		int iTeam = GetClientTeam(client);
		
		if (iTeam == 2)
			BaseComm_SetClientMute(client, true);
		else
			BaseComm_SetClientMute(client, false);
	} else {
		BaseComm_SetClientMute(client, false);
	}
}

stock bool IsValidClient(int client)
{
	if (client >= 1 && client <= MaxClients && IsValidEntity(client) && IsClientConnected(client) && IsClientInGame(client))
		return true;
	return false;
}